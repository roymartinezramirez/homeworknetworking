package com.cynergy.homeworknetworking

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.cynergy.homeworknetworking.model.NoteEntity
import com.cynergy.homeworknetworking.ui.AddNoteActivity
import com.cynergy.homeworknetworking.ui.EditNoteActivity
import com.cynergy.homeworknetworking.ui.adapter.NoteAdapter
import com.cynergy.homeworknetworking.ui.adapter.RecyclerClickListener
import com.cynergy.homeworknetworking.ui.adapter.RecyclerTouchListener
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.Query
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var mDatabase: DatabaseReference
    private var noteAdapter: NoteAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpFirebaseDb()
        ui()
    }

    private fun ui() {
        recyclerViewNotes.layoutManager = LinearLayoutManager(this)
        recyclerViewNotes.setHasFixedSize(true)
        loadNotes()

        btnAddNote.setOnClickListener {
            goToAddNote()
        }

        recyclerViewNotes.addOnItemTouchListener(
            RecyclerTouchListener(this,recyclerViewNotes,
            object : RecyclerClickListener{
                override fun onClick(view: View, position: Int) {
                    val note: NoteEntity?=noteAdapter?.getItem(position)
                    note?.let {
                        gotoNote(note)
                    }
                }

                override fun onLongClick(view: View, position: Int) {}

            })
        )

    }

    private fun gotoNote(note: NoteEntity) {
        val bundle = Bundle()
        bundle.putSerializable("NOTE",note)
        val intent = Intent(this,EditNoteActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun goToAddNote() {
        startActivity(Intent(this,AddNoteActivity::class.java))
    }

    private fun loadNotes() {
        val notesQuery: Query = mDatabase.child("notes")
        val options = FirebaseRecyclerOptions.Builder<NoteEntity>()
            .setQuery(notesQuery,NoteEntity::class.java)
            .build()

        noteAdapter = NoteAdapter(options)
        recyclerViewNotes.adapter = noteAdapter
    }

    private fun setUpFirebaseDb() {
        mDatabase = FirebaseDatabase.getInstance().reference
    }

    override fun onStart() {
        super.onStart()
        noteAdapter?.startListening()
    }

    override fun onStop() {
        super.onStop()
        noteAdapter?.stopListening()
    }
}