package com.cynergy.homeworknetworking.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.cynergy.homeworknetworking.R
import com.cynergy.homeworknetworking.model.NoteEntity
import com.cynergy.homeworknetworking.ui.dialog.NoteDialogFragment
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_edit_note.*
import kotlinx.android.synthetic.main.layout_loading.*

class EditNoteActivity : AppCompatActivity(), NoteDialogFragment.DialogListener {

    private lateinit var mDatabase: DatabaseReference
    private var note: NoteEntity? = null

    private var name: String? = null
    private var desc: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_note)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        verifyExtras()
        mDatabase = FirebaseDatabase.getInstance().reference
        ui()
        populate()
    }

    private fun populate() {
        note?.let {
            etEditName.setText(it?.name)
            etEditDesc.setText(it?.description)
        }
    }

    private fun ui() {
        btnEditEditNote.setOnClickListener {
            if (validateForm()) {
                editNote()
            }
        }

        btnEditDeleteNote.setOnClickListener {
            showNoteDialog()
        }
    }

    private fun showNoteDialog() {
        val noteDialogFragment = NoteDialogFragment()
        val bundle = Bundle()
        bundle.putString("TITLE","¿Quieres eliminar esta nota?")
        bundle.putInt("TYPE",100)

        noteDialogFragment.arguments=bundle
        noteDialogFragment.show(supportFragmentManager,"dialog")
    }

    private fun editNote() {
        showLoading()
        val noteId = note?.id
        val note = NoteEntity(noteId, name, desc)

        noteId?.let {
            mDatabase.child("notes").child(it)
                .updateChildren(note.toMap(), object : DatabaseReference.CompletionListener {
                    override fun onComplete(databaseError: DatabaseError?, ref: DatabaseReference) {
                        hideLoading()
                        databaseError?.let {
                            showErrorMessage(databaseError.message)
                        } ?: run { finish() }
                    }

                })
        }
    }

    private fun showErrorMessage(error: String?) {
        Toast.makeText(this, "Error; $error", Toast.LENGTH_SHORT).show()
    }

    private fun hideLoading() {
        flayLoading.visibility = View.GONE
    }

    private fun showLoading() {
        flayLoading.visibility = View.VISIBLE
    }

    private fun validateForm(): Boolean {
        name = etEditName.text.toString()
        desc = etEditDesc.text.toString()
        if (name.isNullOrEmpty()) {
            return false
        }
        if (desc.isNullOrEmpty()) {
            return false
        }
        return true
    }

    private fun verifyExtras() {
        intent?.extras?.let {
            note = it.getSerializable("NOTE") as NoteEntity
        }
    }

    override fun onPositiveListener(any: Any?, type: Int) {
        note?.let {
            deleteNote(it)
        }
    }

    private fun deleteNote(mNote: NoteEntity) {
        showLoading()
        mNote.id?.let {
            mDatabase.child("notes").child(it)
                .removeValue(object : DatabaseReference.CompletionListener {
                    override fun onComplete(databaseError: DatabaseError?, ref: DatabaseReference) {
                        hideLoading()
                        databaseError?.let {
                            showErrorMessage(databaseError.message)
                        } ?: run { finish() }
                    }

                })
        }
    }

    override fun onNegativeListener(any: Any?, type: Int) {}

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}