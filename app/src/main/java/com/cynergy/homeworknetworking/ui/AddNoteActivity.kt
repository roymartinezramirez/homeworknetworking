package com.cynergy.homeworknetworking.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.cynergy.homeworknetworking.R
import com.cynergy.homeworknetworking.model.NoteEntity
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_add_note.*
import kotlinx.android.synthetic.main.layout_loading.*

class AddNoteActivity : AppCompatActivity() {

    private lateinit var mDatabase: DatabaseReference

    private var name: String? = null
    private var desc: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_note)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mDatabase = FirebaseDatabase.getInstance().reference
        ui()
    }

    private fun ui() {
        btnAddNote.setOnClickListener {
            if (validateForm()) {
                addNote()
            }
        }
    }

    private fun addNote() {
        showLoading()
        val noteId = mDatabase.child("notes").push().key
        val note = NoteEntity(noteId, name, desc)
        noteId?.let {
            mDatabase.child("notes").child(it)
                .setValue(note, object : DatabaseReference.CompletionListener {
                    override fun onComplete(databaseError: DatabaseError?, ref: DatabaseReference) {
                        hideLoading()
                        databaseError?.let { dbError ->
                            showErrorMessage(dbError.message)
                        } ?: run { finish() }
                    }

                })
        }
    }

    private fun showErrorMessage(error: String?) {
        Toast.makeText(this, "Error: $error", Toast.LENGTH_SHORT).show()
    }

    private fun hideLoading() {
        flayLoading.visibility = View.GONE
    }

    private fun showLoading() {
        flayLoading.visibility = View.VISIBLE
    }

    private fun validateForm(): Boolean {
        clearForm()
        name = etAddName.text.toString().trim()
        desc = etAddDesc.text.toString().trim()
        if (name.isNullOrEmpty()) {
            etAddName.error = "Nombre inválido"
            return false
        }
        if (desc.isNullOrEmpty()) {
            etAddDesc.error = "Descripción inválida"
            return false
        }
        return true
    }

    private fun clearForm() {
        etAddName.error = null
        etAddDesc.error = null
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}